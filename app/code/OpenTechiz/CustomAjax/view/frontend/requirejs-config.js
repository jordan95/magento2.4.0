var config = {
    paths: {            
            'owlcarousel': "OpenTechiz_CustomAjax/js/owl.carousel"
            'custom': "OpenTechiz_CustomAjax/js/custom"
        },   
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        }
    }
};